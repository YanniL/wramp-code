.text
.global main
 main:

#setup $evec (where we jump)
movsg $3,$evec
sw $3, old_handler($0)
la $3,handler
movgs $evec,$3

#Enable interrupts
movsg $3,$cctrl
andi $3,$3,0x000f
ori $3,$3,0x42  #Enable timer interrupt
movgs $cctrl,$3

#Setup timmer to generate interrupt
sw $0,0x72000($0)
sw $0,0x72003($0) #akc interrupt
addi $12, $0, 24 #set auto load value in
sw $12, 0x72001($0)
addi $12,$0,0x3		#Timer Enable and auto-restart
sw $12,0x72000($0)

# setup 1st task-------
la $1, task1_pcb
la $2, task2_pcb	#link to task2_pcb
sw $2, pcb_link($1)	
la $2, task1_stack	#link to task1_stack,setup stack point.
sw $2, pcb_sp($1)	
la $2, task1  		#setup the $ear field
sw $2, pcb_ear($1)	#exception address register
sw $3, pcb_cctrl($1)	#setup the $cctrl field

# setup 2nd task-------
la $1, task2_pcb
la $2, task3_pcb		#link to task1_pcb
sw $2, pcb_link($1)
la $2, task2_stack	#link to task2_stack,setup stack point.
sw $2, pcb_sp($1)
la $2, task2		#setup the $ear field
sw $2, pcb_ear($1)	#exception address register
sw $3, pcb_cctrl($1)	#setup the $cctrl field

# setup 3rd task-------
la $1, task3_pcb
la $2, task1_pcb		#link to task1_pcb
sw $2, pcb_link($1)
la $2, task3_stack	#link to task2_stack,setup stack point.
sw $2, pcb_sp($1)
la $2, task3		#setup the $ear field
sw $2, pcb_ear($1)	#exception address register
sw $3, pcb_cctrl($1)	#setup the $cctrl field

#start the first task
la $1, task1_pcb          #load task1_pcb address to $1 
sw $1, current_task($0)	  #save task1_pcb address to current_task


task1:
lw $5, seconds($0)
divi $5, $5,100
sw $5, 0($sp)
jal display 
j task1



display:#####---------Display_1------Display_1-----------

lw $11,0x70003($0)
andi $11,$11, 0x2
beqz $11,display

subui $sp, $sp, 2
sw $ra, 1($sp)
lw $5,2($sp)
addi $5,$5,1
sw $5,0($sp)

addi $4, $0, 0xd 	#carriage
sw $4,0x70000($0)

one_thu:
divi $3,$5,0x258
remi $3, $3,0xa
addi $3,$3,0x30

lw   $11, 0x70003($0)
andi $11, $11, 0x2 #bit set
beqz $11, one_thu
sw $3, 0x70000($0)

one_hund:
divi $3,$5,0x3c
remi $3, $3,0xa
addi $3,$3,0x30

lw   $11, 0x70003($0)
andi $11, $11, 0x2 #bit set
beqz $11, one_hund
sw $3, 0x70000($0)

colon:
lw $11,0x70003($0)
andi $11,$11,0x2
beqz $11,colon
addi $3,$0,0x3a
sw $3,0x70000($0)

ten:
divi $3,$5,0xa
remi $3,$3,6
addi $3,$3,0x30

lw   $11, 0x70003($0)
andi $11, $11, 0x2 #bit set
beqz $11, ten
sw $3, 0x70000($0)
 
one:
remi $3,$5,0xa
addi $3,$3,0x30
lw   $11, 0x70003($0)
andi $11, $11, 0x2 #bit set
beqz $11, one
sw $3, 0x70000($0)


lw $ra, 1($sp)
addui $sp, $sp, 2
jr $ra
####---------Display_1------Display_1-------------


task2:
lw $5,0x73000($0)
sw $5, 0($sp)
jal display2
j task2
####---------Display_2------Display_2-------------
display2:
subui $sp, $sp, 2
sw $ra, 1($sp)
lw $5, 2($sp)

sw $5,0x73003($0)
srli $5,$5,4
sw $5,0x73002($0)
lw $ra, 1($sp)
addui $sp, $sp, 2
jr $ra
####---------Display_2------Display_2-------------

task3:
jal gamejob
j task3

handler: 
movsg $13, $estat #load state register
andi $13,$13,0xFFB0  #No other interrupt enabled
beqz $13, handle
lw $13,old_handler($0)
jr $13

handle:
sw  $0,0x72003($0)  	# Acknowledge the interrupt.

lw $13, seconds($0)	 #get the timer from the memory location	
addi $13, $13, 1         #timer plus 1
sw $13, seconds($0)  #backup

lw $13, time_slice($0)  # Subtract 1 from the timeslice counter
subi $13, $13, 1
sw $13, time_slice($0)

beqz $13, dispatcher   	#timeslice counter is zero then goto dispatcher
rfe			# Otherwise we return from exception




dispatcher:

# Set the timeslice counter to an appropriate value
lw $13, time_slice($0)
addi $13, $0, 100
sw $13, time_slice($0)

# Save context for current task
lw $13, current_task($0)
sw $1, pcb_reg1($13)
sw $2, pcb_reg2($13)
sw $3, pcb_reg3($13)
sw $4, pcb_reg4($13)
sw $5, pcb_reg5($13)
sw $6, pcb_reg6($13)
sw $7, pcb_reg7($13)
sw $8, pcb_reg8($13)
sw $9, pcb_reg9($13)
sw $10, pcb_reg10($13)
sw $11, pcb_reg11($13)
sw $12, pcb_reg12($13)
sw $ra, pcb_ra($13)
sw $sp, pcb_sp($13)

# special case for $13
movsg $1, $ers
sw $1, pcb_reg13($13)
movsg $1, $ear
sw $1, pcb_ear($13)
movsg $1, $cctrl
sw $1, pcb_cctrl($13)


# Select the next task to run
lw $13, pcb_link($13)
sw $13, current_task($0)



# Load context for next task

lw $1, pcb_reg13($13)
movgs $ers, $1

lw $1, pcb_ear($13)
movgs $ear, $1

lw $1, pcb_cctrl($13)
movgs $cctrl, $1

lw $1, pcb_reg1($13)
lw $2, pcb_reg2($13)
lw $3, pcb_reg3($13)
lw $4, pcb_reg4($13)
lw $5, pcb_reg5($13)
lw $7, pcb_reg7($13)
lw $8, pcb_reg8($13)
lw $9, pcb_reg9($13)
lw $10, pcb_reg10($13)
lw $11, pcb_reg11($13)
lw $12, pcb_reg12($13)
lw $ra, pcb_ra($13)
lw $sp, pcb_sp($13)

rfe		# Return to the new task




#**************************************

.data
seconds:
   .word 0
time_slice:
   .word 100

.bss
old_handler:
.word

task1_pcb:
.space 18

task2_pcb:
.space 18

task3_pcb:
.space 18

current_task:
.word

.space 100
task1_stack:

.space 100
task2_stack:

.space 100
task3_stack:

.equ pcb_link,0
.equ pcb_reg1,1
.equ pcb_reg2,2
.equ pcb_reg3,3
.equ pcb_reg4,4
.equ pcb_reg5,5
.equ pcb_reg6,6
.equ pcb_reg7,7
.equ pcb_reg8,8
.equ pcb_reg9,9
.equ pcb_reg10,10
.equ pcb_reg11,11
.equ pcb_reg12,12
.equ pcb_reg13,13
.equ pcb_sp,14
.equ pcb_ra,15
.equ pcb_ear,16
.equ pcb_cctrl,17












